'''Extremely simple backend API.
'''

from os import getenv

import mysql.connector
from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"])


@app.get("/")
def read_root():
    '''This is the only endpoint in our backend API.

    It reads some data from the DB and returns it in json format.'''
    conn = mysql.connector.connect(host=getenv("DB_HOST"),
                                   user=getenv("DB_USR"),
                                   password=getenv("DB_PASS"),
                                   database=getenv("DB_DB"))
    if not conn.is_connected():
        raise HTTPException(status_code=500, detail="Couldn't connect to DB!")
    cur = conn.cursor()
    cur.execute("SELECT data FROM backend_data")
    lines = cur.fetchall()
    conn.close()
    return {"data": lines}
